package br.com.itau;

public class Main {

    public static void Main(String[] args){
        Agenda agenda = new Agenda();

        agenda.adicionarContato();
        agenda.removerContato();
        agenda.mostrarAgenda();
    }
}
